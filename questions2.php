<?php
$cookies = json_decode($_COOKIE['quiz'], true);
if (isset($_POST['prev'])) {
    $merge = array_merge($cookies, $_POST);
    $cookies = $merge;
    setcookie("quiz", json_encode($cookies), time() + 3600);
    header('location: questions1.php');
} else if (isset($_POST['submit'])) {
    $merge = array_merge($cookies, $_POST);
    $cookies = $merge;
    setcookie("quiz", json_encode($cookies), time() + 3600);
    header('location: result.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trắc nghiệm online</title>
</head>

<body style="margin-left:50px; margin-top:10px">

    <form method="POST">
        <h1>
            Bài trắc nghiệm
        </h1>

        <?php
        $answers = array(
            "Câu 6" => array('A' => "array_to_str()", 'B' => "array_str()", 'C' => "implode()", 'D' => "explode()"),
            "Câu 7" => array('A' => "array_unshift()", 'B' => "into_array()", 'C' => "inend_array()", 'D' => "array_push()"),
            "Câu 8" => array('A' => "protected", 'B' => "abstract", 'C' => "public", 'D' => "private"),
            "Câu 9" => array('A' => "array_rand()", 'B' => "array_random()", 'C' => "random_array()", 'D' => "rand_array()"),
            "Câu 10" => array('A' => "mysql_connect(\"localhost\");", 'B' => "connect_mysql(\"localhost\");", 'C' => "mysql_open(\"localhost\");", 'D' => "dbopen(\"localhost\");")
        );
        $questions = array(
            "Chức năng \"Chuyển một mảng thành một chuỗi\" là chức năng của hàm nào trong các hàm sau đây?",
            "Trong PHP, hàm nào dùng để thêm phần tử vào cuối mảng?",
            "Trong các điều khiển truy cập sau đây, điều khiển nào xác định rằng một tính năng có thể được truy cập bởi tất cả các lớp khác?",
            "Hàm nào sau đây dùng để lấy 1 hoặc nhiều giá trị ngẫu nhiên từ mảng php?",
            "Kết nối db nào là đúng?"
        );
        ?>

        <div>
            <?php
            $index = 6;
            foreach (array_keys($answers) as $question) {
                echo "<p>" . $question . ': ' . $questions[$index - 6] . "</p>";
                foreach (array_keys($answers[$question]) as $selection) {
                    echo "<input value='" . $selection . "' id='" . $question . $selection . "' type='radio' name='Q" . $index . "' style='margin-bottom: 30px'";
                    echo isset($cookies['Q' . $index]) && $cookies['Q' . $index] == $selection ? " checked" : "";
                    echo " /> ";
                    echo "<label for='" . $question . $selection . "'>" . $selection . '. ' . $answers[$question][$selection] . "</label><br>";
                }
                $index++;
            }
            ?>
        </div>
        <button type='submit' name='prev'>Về trước</button>
        <button type='submit' name='submit'>Nộp bài</button>
    </form>
</body>

</html>