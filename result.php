<?php
$cookies = json_decode($_COOKIE['quiz'], true);
unset($_COOKIE['quiz']);
setcookie('quiz', null, time() - 3600);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trắc nghiệm online</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #eeeeee;
        }
    </style>
</head>

<body style="margin-left:50px; margin-top:10px">
    <h1>
        Kết quả bài trắc nghiệm
    </h1>

    <?php
    const result = array('B', 'A', 'B', 'A', 'C', 'C', 'D', 'C', 'A', 'A');
    ?>
    <table style="width:80%; margin: 20px">
        <tr>
            <th>Câu hỏi</th>
            <?php
            for ($i = 1; $i < 11; $i++) {
                echo "<th>Câu " . $i . "</th>";
            }
            ?>
        </tr>
        <tr>
            <th>Trả lời</th>
            <?php
            $score = 0;
            for ($i = 1; $i < 11; $i++) {
                echo "<td>";
                if (isset($cookies["Q" . $i])) {
                    echo $cookies["Q" . $i];
                    if ($cookies["Q" . $i] == result[$i - 1]) {
                        $score++;
                    }
                }
                echo "</td>";
            }
            ?>
        </tr>
        <tr>
            <th>Đáp án</th>
            <?php
            for ($i = 0; $i < 10; $i++) {
                echo "<td>" . result[$i] . "</td>";
            }
            ?>
        </tr>
    </table>
    <h2>Điểm của bạn là: <?php echo $score ?></h2>
    <h2>
        <?php
        echo $score < 4 ? "Bạn quá kém, cần ôn tập thêm"
            : ($score <= 7 ? "Cũng bình thường"
                : "Sắp sửa làm được trợ giảng lớp PHP");
        ?>
    </h2>
</body>

</html>